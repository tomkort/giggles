#pragma once

#include "Giggles/Giggles.h"

class MainScene : public ggl::Scene
{
public:

    // Get instance of the App object
    ggl::App& app = ggl::App::getInstance();

    void update();
    void init();
    void clean();

};
