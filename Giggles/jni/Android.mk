LOCAL_PATH := $(call my-dir)

FILE_LIST := $(wildcard $(LOCAL_PATH)/../Giggles/src/**/*.cpp)

include $(CLEAR_VARS)

LOCAL_MODULE := libGiggles
LOCAL_CFLAGS := -Werror
LOCAL_SRC_FILES := main.cpp MainScene.cpp $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_LDLIBS := -llog -landroid -lEGL -lGLESv2
LOCAL_STATIC_LIBRARIES := android_native_app_glue

LOCAL_C_INCLUDES += Giggles/include/
LOCAL_C_INCLUDES += ext/glm/glm/

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
