
#include <jni.h>

#include "../native_app_glue/android_native_app_glue.h"

#include "Giggles/Giggles.h"

#include "MainScene.h"

static void ggl_main_redirect_cmd(struct android_app* state, int32_t cmd)
{
    ggl::App* app = (ggl::App*)state->userData;
    switch (cmd) {
    case APP_CMD_INIT_WINDOW:
        if (app->androidState->window != NULL) {
            ggl::Display::getInstance().init();
        }
        break;
    case APP_CMD_TERM_WINDOW:
        ggl::Display::getInstance().destroy();
        break;
    }
}

static int32_t ggl_main_redirect_input(struct android_app* app, AInputEvent* event)
{
    return 0;
}

void android_main(struct android_app* state)
{

    app_dummy();

    // Initialise core components

    ggl::App& app = ggl::App::getInstance();

    app.addScene("MainScene", new MainScene());
    app.setScene("MainScene");


    state->onAppCmd = ggl_main_redirect_cmd;
    state->onInputEvent = ggl_main_redirect_input;

    app.androidState = state;

    state->userData = &app;

    while (1) {
        int ident;
        int events;
        struct android_poll_source* source;

        while ((ident = ALooper_pollAll(0, nullptr, &events, (void**)&source)) >= 0) {
            if (source != nullptr) {
                source->process(state, source);
            }
            if (state->destroyRequested != 0) {
                app.display.destroy();
                return;
            }
        }

        app.update();
        
    }

}
