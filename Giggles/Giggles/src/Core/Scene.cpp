#include "Giggles/Core/Scene.h"

namespace ggl {

    Scene::Scene()
    {
    }


    Scene::~Scene()
    {
    }

    GameObject& Scene::addGameObject(const std::string& name, GameObject& gameobject)
    {
    	gameobjects[name] = &gameobject;
    	return *gameobjects[name];
    }

    GameObject& Scene::addGameObject(const std::string& name, GameObject* gameobject)
    {
    	gameobjects[name] = gameobject;
    	return *gameobjects[name];
    }

}
