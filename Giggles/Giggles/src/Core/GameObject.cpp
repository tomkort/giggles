#include "Giggles/Core/GameObject.h"

namespace ggl {

    GameObject::GameObject()
    {
    }


    GameObject::~GameObject()
    {
    }


    Component& GameObject::addComponent(const std::string& name, Component* component)
    {
        components[name] = component;
        return *components[name];
    }


}
