#include "Giggles/Core/App.h"
#include "Giggles/Graphics/Display.h"
namespace ggl {

    void App::setScene(const std::string& name)
    {
        if (current_scene != nullptr)
            (*current_scene).clean();
        current_scene = scene_map[name];
        (*current_scene).init();
    }

    Scene& App::addScene(const std::string& name, Scene& scene)
    {
        scene_map[name] = &scene;
        return scene;
    }

    Scene& App::addScene(const std::string& name, Scene* scene)
    {
        scene_map[name] = scene;
        return *scene;
    }

    void App::update()
    {
        (*current_scene).update();
        display.swap();
    }

}

