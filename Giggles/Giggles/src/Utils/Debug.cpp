#include "Giggles/Utils/Debug.h"
#include <EGL/egl.h>

namespace ggl {

    /**
     * Prints debug message tagged as Info to logcat
     *
     * @param   message Message to print. Can be formatted as printf format string.
     * @param   ...     Variables to print as defined in format string.
     */
    void Debug::Log(LogTag tag, const char* message, ...)
    {
        va_list arglist;
        va_start(arglist, message);
        (void)__android_log_vprint((int)tag, "Giggles", message, arglist);
        va_end(arglist);
    }

    ///**
    // * Prints debug message tagged as Warning to logcat
    // *
    // * @param   message Message to print. Can be formatted as printf format string.
    // * @param   ...     Variables to print as defined in format string.
    // */
    //void Debug::LogWarning(const char* message, ...)
    //{
    //    va_list arglist;
    //    va_start(arglist, message);
    //    (void)__android_log_vprint(ANDROID_LOG_WARN, "Giggles", message, arglist);
    //    va_end(arglist);
    //}

    ///**
    // * Prints debug message tagged as Error to logcat
    // *
    // * @param   message Message to print. Can be formatted as printf format string.
    // * @param   ...     Variables to print as defined in format string.
    // */
    //void Debug::LogError(const char* message, ...)
    //{
    //    va_list arglist;
    //    va_start(arglist, message);
    //    (void)__android_log_vprint(ANDROID_LOG_ERROR, "Giggles", message, arglist);
    //    va_end(arglist);
    //}

    /**
     * Checks if there is EGL Error and prints debug message and error id
     * tagged as Error to logcat
     *
     * @param   additional_message  Message to print.
     */
    void Debug::CheckGLError(LogTag tag, const char* additional_message)
    {
        EGLint error = eglGetError();
        if (error != EGL_SUCCESS)
        {
            Log(tag, "%s: EGLError %d\n", additional_message, error);
			
        }
    }

}