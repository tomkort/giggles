#include "Giggles/Graphics/Display.h"

#include "Giggles/Utils/Debug.h"
#include "Giggles/Core/App.h"

#include <jni.h>
#include <android/native_window.h>

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <inttypes.h>

namespace ggl {

    void Display::init()
    {
        EGLint num_config;
        EGLint visual_id;
        EGLint config_attrib_list[] = {
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_ALPHA_SIZE, 8,
            EGL_DEPTH_SIZE, 8,
            EGL_STENCIL_SIZE, 8,
            EGL_NONE
        };

        EGLint context_attrib_list[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE
        };

        app = App::getInstance().androidState;

        display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		Debug::CheckGLError(LogTag::ERROR, "GetDisplay");

        eglInitialize(display, 0, 0);
		Debug::CheckGLError(LogTag::ERROR, "Initialize Display");

        eglChooseConfig(display, config_attrib_list, &config, 1, &num_config);
		Debug::CheckGLError(LogTag::ERROR, "ChooseConfig");

        eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &visual_id);
		Debug::CheckGLError(LogTag::ERROR, "Get VISUAL_ID");

        ANativeWindow_setBuffersGeometry(app->window, 0, 0, visual_id);


        surface = eglCreateWindowSurface(display, config, app->window, 0);
		Debug::CheckGLError(LogTag::ERROR, "Create Window");

        context = eglCreateContext(display, config, EGL_NO_CONTEXT, context_attrib_list);
		Debug::CheckGLError(LogTag::ERROR, "Create Context");

        if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
			Debug::CheckGLError(LogTag::ERROR, "MakeCurrent");
            Debug::Log(LogTag::ERROR, "MakeCurrent failed!");
        }

        EGLint w;
        EGLint h;

        eglQuerySurface(display, surface, EGL_WIDTH, &w);
        eglQuerySurface(display, surface, EGL_HEIGHT, &h);

        this->width = w;
        this->height = h;

		glEnable(GL_BLEND);
		
		shader.loadDefault();

		Debug::Log(LogTag::INFO, (const char*)glGetString(GL_VERSION));
        Debug::Log(LogTag::INFO, "%i, %i", this->width, this->height);

    }

    void Display::destroy()
    {
        Debug::Log(LogTag::INFO, "Destroy");
        if (display != EGL_NO_DISPLAY) {
            eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
            if (display != EGL_NO_DISPLAY) {
                eglDestroyContext(display, context);
            }
            if (display != EGL_NO_DISPLAY) {
                eglDestroySurface(display, surface);
            }
            eglTerminate(display);
            display = EGL_NO_DISPLAY;
            context = EGL_NO_CONTEXT;
            surface = EGL_NO_SURFACE;
        }
    }

    void Display::swap()
    {
        eglSwapBuffers(display, surface);
    }

    void Display::clearColor(float red, float green, float blue, float alpha)
    {
        glClearColor(red, green, blue, alpha);
        glClear(GL_COLOR_BUFFER_BIT);
    }
}
