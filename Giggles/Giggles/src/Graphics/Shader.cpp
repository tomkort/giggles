#include "Giggles/Graphics/Shader.h"

namespace ggl {

	Shader::Shader()
	{
	}


	Shader::~Shader()
	{
	}

	void Shader::loadDefault()
	{
		// TODO: CHECK FOR ERRORS!!!!!!
		vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShaderId, 1, &defaultVertexShader, nullptr);
		glCompileShader(vertexShaderId);

		GLint vertex_compiled;
		glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &vertex_compiled);
		if (vertex_compiled != GL_TRUE) {
			Debug::Log(LogTag::ERROR, "vertex shader compile failed!");

            GLsizei length = 0;
            GLchar message[1024];
            glGetShaderInfoLog(vertexShaderId, 1024, &length, message);
            Debug::Log(LogTag::ERROR, "%s", &message);
		}

		fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShaderId, 1, &defaultFragmentShader, nullptr);
        glCompileShader(fragmentShaderId);

		GLint fragment_compiled;
		glGetShaderiv(fragmentShaderId, GL_COMPILE_STATUS, &fragment_compiled);
		if (fragment_compiled != GL_TRUE) {
			Debug::Log(LogTag::ERROR, "fragment shader compile failed!");
            
            GLsizei length = 0;
            GLchar message[1024];
            glGetShaderInfoLog(fragmentShaderId, 1024, &length, message);
            Debug::Log(LogTag::ERROR, "%s", &message);
		}

		programId = glCreateProgram();

		glAttachShader(programId, vertexShaderId);
		glAttachShader(programId, fragmentShaderId);

		glLinkProgram(programId);

	}

}
