
#include "../Core/Component.h"
#include <string>
#include <glm.hpp>

namespace ggl
{

    class Sprite : public Component
    {
    public:
        glm::vec2 position;
        glm::vec3 color;
    private:
    };

}
