#pragma once

#include <jni.h>
#include "../../native_app_glue/android_native_app_glue.h"

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include "Giggles/Graphics/Shader.h"

namespace ggl {

    /**
     * Class that takes care of EGL context initialization and management.
     */

    class Display
    {
    public:

        // Public members

        int width; // Width of the display in pixels
        int height; // Height of the display in pixels

        /**
        * Get instance of Display singleton instance.
        *
        * @return Reference to Display instance.
        */
        static Display& getInstance()
        {
            static Display instance;
            return instance;
        }

        /**
         * Initializes the display creating EGL context and
         * OpenGL ES 2 surface
         */
        void init();

        /**
         * Destroy GL contexts and clean up.
         */
        void destroy();

        /**
         * Bring framebuffer to front.
         */
        void swap();

        /**
         * Clear screen with color.
         *
         * @param   red     Red component
         * @param   green   Green component
         * @param   blue    Blue component
         * @param   alpha   Transparency
         */
        void clearColor(float red, float green, float blue, float alpha);

    private:
        // Singleton, so private constructor/destructor
        Display(){};
        ~Display(){};

        // NonCopyable
        Display(const Display&);
        Display& operator=(const Display&);

        // Private members
		Shader shader;
        android_app* app;

        EGLNativeWindowType window;
        EGLDisplay display;
        EGLSurface surface;
        EGLContext context;
        EGLConfig config;


    };

}
