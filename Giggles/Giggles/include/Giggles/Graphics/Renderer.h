#pragma once

namespace ggl {

    class Renderer
    {
    public:

	private:
		// Singleton
		Renderer(){};
		~Renderer(){};

		// Noncopyable
		Renderer(const Renderer& renderer);
		Renderer& operator=(const Renderer& renderer);
    };

}