#pragma once

#include "Giggles/Utils/Debug.h"

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <string>

namespace ggl {

	class Shader
	{
	private:

		const char* defaultVertexShader =
			"attribute mediump vec3 position;"
			"attribute mediump vec4 color;"
			"attribute mediump vec2 tex_coord;"
			
			"uniform mediump mat4 projection;"
			
			"varying mediump vec4 out_color;"
			"varying mediump vec2 out_tex_coord;"
			
			"void main(void)"
			"{"
			"	out_color = color;"
			"	out_tex_coord = tex_coord;"
			"	gl_Position = vec4(position, 0.0);"
			"}";

		const char* defaultFragmentShader =
			"varying mediump vec2 tex_coords;"
			"varying mediump vec4 color;"
			
			"uniform sampler2D texture;"
			
			"void main(void)"
			"{"
			"	gl_FragColor = texture2D(texture, tex_coords);"
			"}";

	public:
		Shader();
		~Shader();

		void loadDefault();

		void bind();
		void attach();

	private:
		GLuint vertexShaderId;
		GLuint fragmentShaderId;
		GLuint programId;

	};

}
