#pragma once

#include <jni.h>
#include <errno.h>
#include <android/log.h>

#include <cstdarg>

namespace ggl {

	enum class LogTag{ABORT   = ANDROID_LOG_FATAL,
					  ERROR   = ANDROID_LOG_ERROR,
					  WARNING = ANDROID_LOG_WARN,
					  INFO    = ANDROID_LOG_INFO};

    namespace Debug {
        void Log(LogTag tag, const char* message, ...);
        //void LogWarning(const char* message, ...);
        //void LogError(const char* message, ...);
        void CheckGLError(LogTag tag, const char* additional_message);

    }

}