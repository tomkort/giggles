#pragma once

#include "GameObject.h"
#include <unordered_map>
#include <string>

namespace ggl {

    class Scene
    {
    public:
        Scene();
        virtual ~Scene();
        
        /**
         * Virtual update function. Must be implemented in derived class.
         * Get updated by App at every frame.
         */
        virtual void update() = 0;

        /**
         * Virtual initialisations function. Must be implemented in derived class.
         * Gets called when scene is accessed first time or after scene change.
         */
        virtual void init() = 0;

        /**
         * Virtual uninitialisation function. Must be implemented in derived class.
         * Gets called when scene is changed to another or program ends.
         */
        virtual void clean() = 0;
        
        GameObject& addGameObject(const std::string& name, GameObject& gameobject);

        GameObject& addGameObject(const std::string& name, GameObject* gameobject);

    private:
        std::unordered_map<std::string, GameObject*> gameobjects;

    };

}
