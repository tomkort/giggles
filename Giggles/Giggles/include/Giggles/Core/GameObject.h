#pragma once

#include "Giggles/Core/Component.h"
#include <unordered_map>

namespace ggl {

    class GameObject
    {
    public:
        GameObject();
        virtual ~GameObject();

        /**
         * Add component to gameobject.
         *
         * @param   name    Name for the component.
         * @param   component   Pointer to the component.
         * @return  Reference to added component.
         */
        Component& addComponent(const std::string& name, Component* component);
        
        /**
         * Virtual update method. Gets called at every frame.
         * Must be impelemented in derived class.
         */
        virtual void update() = 0;
        
        /**
         * Virtual initialisation method. Gets called when object is firs accessed.
         */
        virtual void init() = 0;
        
        /**
         * Virtual unitialisation method. Gets called when object deleted.
         */
        virtual void clean() = 0;

    private:
        std::unordered_map<std::string, Component*> components;
    };

}
