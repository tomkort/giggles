#pragma once

#include "../../native_app_glue/android_native_app_glue.h"

#include <string>
#include <memory>
#include <unordered_map>

#include "Giggles/Core/Scene.h"
#include "Giggles/Graphics/Display.h"
#include "Giggles/Utils/Debug.h"




namespace ggl {

    class App
    {
    public:

        // Public members
        android_app* androidState = nullptr; // Application info provided by googles
                          // native app glue.

        float deltaTime = 0.0f;

        Display& display;

        /**
         * Change current scene to another.
         * @param   name    Name of the scene to change to.
         */
        void setScene(const std::string& name);

        /**
         * Add scene to application. Scenes are kept in unordered list.
         * Key is std::string and value is pointer to the scene instance.
         *
         * @param   name    Name of the scene.
         * @param   scene   Address to the scene instance.
         */
        Scene& addScene(const std::string& name, Scene& scene);

        /**
        * Add scene to application. Scenes are kept in unordered list.
        * Key is std::string and value is pointer to the scene instance.
        *
        * @param   name    Name of the scene.
        * @param   scene   Pointer to the scene instance.
        */
        Scene& addScene(const std::string& name, Scene* scene);
        /**
         * Updates application after every frame
         */
        void update();
        

        void clean();
        /**
         * Get instance of App singleton instance.
         *
         * @return Reference to App instance.
         */
        static App& getInstance()
        {
            static App instance;
            return instance;
        }

    private:
        // Singleton
        App():
            display(Display::getInstance())
        {};
        ~App(){};

        // NonCopyable
        App(const App&);
        App& operator=(const App&);

        std::unordered_map < std::string, Scene* > scene_map;
        Scene* current_scene = nullptr;

    };

}
