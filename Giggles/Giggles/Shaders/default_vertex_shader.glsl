

attribute mediump vec2 position;
attribute mediump vec4 color;
attribute mediump vec2 tex_coord;

uniform mediump mat4 projection;

varying mediump vec4 out_color;
varying mediump vec2 out_tex_coord;

void main(void)
{
	out_color = color;
	out_tex_coord = tex_coord;
	gl_Position = vec3(position, 0);
}
