
varying vec2 tex_coords;
varying vec4 color;

uniform sampler2D texture;

void main(void)
{
	gl_FragColor = texture2D(texture, tex_coords);
}